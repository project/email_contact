# Email Contact

This module provides display formatters for the email field to display
email as a link to the contact form, or as an inline contact form.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/email_contact).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/email_contact).


## Table of contents

- Requirements
- Installation
- Configuration
- How to use
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.
Depends on the core E-mail module.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module currently provides no configuration options.


## HOW TO USE

Since there are no extra settings, so to see that working you can follow these
steps:

1. Add the `"email"` field anywhere you want, for example: `"Basic Page"`
2. Go to Manage Display at:
   Configuration » Structure » Content types » < CONTENT-TYPE > Manage display
3. In the `"Format"` column you will see new options, for example:
    1. Email contact inline
    2. Email contact link
4. You can save that and see it working in the front-end


## Maintainers

- Bálint Nagy - [nagy.balint](https://www.drupal.org/u/nagybalint)
- Dénes Szabó - [Denes.Szabo](https://www.drupal.org/u/denesszabo)
- Art Williams - [artis](https://www.drupal.org/u/artis)
- Carlos E Basqueira - [cebasqueira](https://www.drupal.org/u/cebasqueira)
