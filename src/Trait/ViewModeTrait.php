<?php

namespace Drupal\email_contact\Trait;

use Drupal\Core\Entity\EntityInterface;

trait ViewModeTrait {
  /**
   * Check if full view mode is not enabled returned default view mode.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity object.
   * @param string $view_mode
   *   Entity view mode.
   *
   * @return string
   *   View mode.
   */
  private function getViewMode(EntityInterface $entity, string $view_mode) : string {
    if ($this->entityTypeManager->getStorage('entity_view_display')->load($entity->getEntityTypeId() . '.' . $entity->bundle() . '.' . $view_mode) == NULL) {
      return 'default';
    }
    return $view_mode;
  }

}
