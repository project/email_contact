<?php

namespace Drupal\email_contact\Form;

use Drupal\Core\Ajax\AfterCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Render\Markup;
use Drupal\email_contact\Event\AjaxEmailContactCommandEvent;

/**
 * Default class to build the Contact Form.
 */
class ContactForm extends FormBase {

  /**
   * The entity type machine name.
   *
   * @var string|null
   */
  protected $entityType;

  /**
   * String for the entity ID.
   *
   * @var string|null
   */
  protected $entityId;

  /**
   * String for the field name.
   *
   * @var string|null
   */
  protected $fieldName;

  /**
   * Array with the field settings.
   *
   * @var array|null
   */
  protected $fieldSettings;

  /**
   * Contact Form constructor.
   */
  public function __construct($entityType = NULL, $entityId = NULL, $fieldName = NULL, $fieldSettings = NULL) {
    $this->entityType = $entityType;
    $this->entityId = $entityId;
    $this->fieldName = $fieldName;
    $this->fieldSettings = $fieldSettings;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'email_contact_mail_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // $this->entityId = $id;
    $user = \Drupal::currentUser();
    $emails = email_contact_get_emails_from_field($this->entityType, $this->entityId, $this->fieldName);
    $form['emails'] = [
      '#type' => 'value',
      '#value' => serialize($emails),
    ];
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your name'),
      '#maxlength' => 255,
      '#default_value' => $user->id() ? $user->getDisplayName() : '',
      '#required' => TRUE,
    ];
    $form['mail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your e-mail address'),
      '#maxlength' => 255,
      '#default_value' => $user->id() ? $user->getEmail() : '',
      '#required' => TRUE,
    ];
    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#maxlength' => 255,
      '#required' => TRUE,
    ];
    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send e-mail'),
    ];

    if (!empty($this->fieldSettings['modal'])) {
      $form['submit']['#attributes']['class'][] = 'use-ajax';
      $form['submit']['#ajax'] = [
        'callback' => [$this, 'ajaxSubmit'],
        'event' => 'click',
      ];
      $form['submit']['#attached']['library'][] = 'core/drupal.dialog.ajax';
      $form['#prefix'] = '<div id="email_contact_modal_form">';
      $form['#suffix'] = '</div>';
    }

    if (!$form_state->get('settings')) {
      $form_state->set('settings', $this->fieldSettings);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!\Drupal::service('email.validator')->isValid($form_state->getValue('mail'))) {
      $form_state->setErrorByName('mail', $this->t('You must enter a valid e-mail address.'));
    }
    if (preg_match("/\r|\n/", $form_state->getValue('subject'))) {
      $form_state->setErrorByName('subject', $this->t('The subject cannot contain linebreaks.'));
      $msg = 'Email injection exploit attempted in email form subject: @subject';
      $this->logger('email_contact')->notice($msg, ['@subject' => $form_state['values']['subject']]);
    }
  }

  /**
   * Submit contact form dialog #ajax callback.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An AJAX response that display validation error messages or represents a
   *   successful submission.
   */
  public function ajaxSubmit(array &$form, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) {
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -1000,
      ];
      $form['#sorted'] = FALSE;
      return (new AjaxResponse())->addCommand(
        new ReplaceCommand('#email_contact_modal_form', $form)
      );
    }

    $this->sendMessage($form, $form_state);
    $messages = ['#type' => 'status_messages'];

    $response = new AjaxResponse();
    // Pipe status messages to the main page.
    $response->addCommand(new AfterCommand('[data-drupal-messages-fallback]', $messages));
    // Allow modules to alter or replace the Ajax command.
    $event = new AjaxEmailContactCommandEvent(new CloseModalDialogCommand(), [
      'instance' => 'email_contact.modal.close',
    ]);
    $event_dispatcher = \Drupal::service('event_dispatcher');
    $event_dispatcher->dispatch($event);

    $response->addCommand($event->getCommand());
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!empty($this->fieldSettings['modal'])) {
      return;
    }

    $this->sendMessage($form, $form_state);

    $redirect = '/';
    if (!empty($this->fieldSettings['redirection_to'])) {
      switch ($this->fieldSettings['redirection_to']) {
        case 'current':
          $redirect = NULL;
          break;

        case 'custom':
          $redirect = $this->fieldSettings['custom_path'];
          break;

        default:
          // @todo $form_state['redirect'] = $path.
          break;

      }
    }
    if ($redirect) {
      $url = Url::fromUserInput($redirect);
      $form_state->setRedirectUrl($url);
    }
  }

  /**
   * Prepares and sends the message.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  protected function sendMessage(array &$form, FormStateInterface $form_state): void {
    $emails = unserialize($form_state->getValue('emails'));
    // E-mail address of the sender: as the form field is a text field,
    // all instances of \r and \n have been automatically stripped from it.
    $reply_to = $form_state->getValue('mail');
    $settings = $form_state->get('settings');

    $params['subject'] = $form_state->getValue('subject');
    $params['name'] = $form_state->getValue('name');
    $params['default_message'] = $settings['default_message'];
    $message = '';

    if ($settings['include_values']) {
      $message .= 'Name: ' . $params['name'] . '<br/>' . 'Email: ' . $reply_to . '<br/>';
    }
    $message .= '<br/>Message: ' . $form_state->getValue('message');
    $params['message'] = Markup::create($message);

    // Send the e-mail to the recipients.
    $mailManager = \Drupal::service('plugin.manager.mail');
    $to = implode(', ', $emails);
    $module = 'email_contact';
    $key = 'contact';
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $send = TRUE;
    $result = $mailManager->mail($module, $key, $to, $langcode, $params, $reply_to, $send);
    if (empty($result['result'])) {
      $this->messenger()->addError($this->t('There was a problem sending your message and it was not sent.'));
    }
    else {
      $this->messenger()->addStatus($this->t('Your message has been sent.'));
      $msg = 'Email sent from: @replyto to: @to about: "@subject" containing: "@message"';
      $this->logger('email_contact')->notice($msg, [
        '@name' => $params['name'],
        '@replyto' => $reply_to,
        '@to' => $to,
        '@subject' => $params['subject'],
        '@message' => $params['message'],
      ]);
    }
  }

}
