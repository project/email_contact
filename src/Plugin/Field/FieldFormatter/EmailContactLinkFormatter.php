<?php

namespace Drupal\email_contact\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Url;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\email_contact\Trait\ViewModeTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Plugin implementation of the 'email_contact_link' formatter.
 */
#[FieldFormatter(id: 'email_contact_link', label: new TranslatableMarkup('Email contact link'), field_types: ['email'])]
class EmailContactLinkFormatter extends FormatterBase {

  use ViewModeTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a EmailContactLinkFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $element['redirection_to'] = [
      '#type' => 'hidden',
      '#value' => 'custom',
    ];

    $element['custom_path'] = [
      '#type' => 'hidden',
      '#value' => '',
    ];

    $element['include_values'] = [
      '#title' => t('Display all field values in email body'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('include_values'),
    ];

    $element['default_message'] = [
      '#title' => t('Additional message in email body'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('default_message'),
    ];

    $element['link_text'] = [
      '#title' => t('Link text'),
      '#type' => 'textfield',
      '#default_value' => $this->getSettings()['link_text'],
    ];

    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $element['token_help'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => ['node'],
      ];
    }

    $element['modal'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Modal'),
      '#default_value' => $this->getSetting('modal'),
    ];

    $element['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The form page or modal title'),
      '#description' => $this->t("If omitted, the entity label followed by '- Email Contact' is used."),
      '#default_value' => $this->getSetting('title'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'link_text' => t('Contact person by email'),
      'redirection_to' => 'custom',
      'custom_path' => '',
      'include_values' => 1,
      'default_message' => '',
      'modal' => FALSE,
      'title' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = [];
    $summary[] = $this->getSetting('modal') ? t('Displays a contact form in a Modal Dialog.') : t('Displays a link to a contact form.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $modal = [];
    if ($this->getSetting('modal')) {
      $modal = [
        '#attributes' => [
          'class' => ['use-ajax'],
        ],
        '#attached' => [
          'library' => [
            'core/drupal.dialog.ajax',
          ],
        ],
      ];
    }

    foreach ($items as $delta => $item) {
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = $item->getEntity();
      if (!$entity->id()) {
        break;
      }
      $elements[$delta] = [
        '#type' => 'link',
        '#title' => $this->getSetting('link_text'),
        '#url' => Url::fromRoute('email_contact.form', [
          'entity_type' => $entity->getEntityTypeId(),
          'entity' => $entity->id(),
          'field_name' => $items->getName(),
          'view_mode' => $this->getViewMode($entity, $this->viewMode),
        ]),
      ] + $modal;
      break;
    }

    return $elements;
  }

}
