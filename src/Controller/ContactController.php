<?php

declare(strict_types = 1);

namespace Drupal\email_contact\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\email_contact\Event\AjaxEmailContactCommandEvent;
use Drupal\email_contact\Form\ContactForm;
use Drupal\email_contact\Plugin\Field\FieldFormatter\EmailContactLinkFormatter;
use Drupal\email_contact\Trait\ViewModeTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Controller for the email_contact module.
 */
class ContactController extends ControllerBase {

  use ViewModeTrait;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * Constructs an ContactController instance.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   */
  public function __construct(LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager, FormBuilderInterface $form_builder, EventDispatcherInterface $event_dispatcher) {
    $this->logger = $logger;
    $this->entityTypeManager = $entity_type_manager;
    $this->formBuilder = $form_builder;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory')->get('action'),
      $container->get('entity_type.manager'),
      $container->get('form_builder'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function accessCheck($entity_type, EntityInterface $entity, $field_name, $view_mode, AccountInterface $account) {
    if ($entity) {
      $entity_access = $entity->access('view', $account, TRUE);

      if (!$entity_access->isAllowed()) {
        return AccessResult::forbidden();
      }

      if (!$entity->hasField($field_name)) {
        return AccessResult::forbidden();
      }

      return $entity->get($field_name)->access('view', $account, TRUE);
    }

    return AccessResult::forbidden();
  }

  /**
   * Provides a title callback for the 'email_contact.form' route.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The field host entity.
   * @param string $field_name
   *   The field name.
   * @param string $view_mode
   *   The view mode.
   *
   * @return \Drupal\Component\Render\MarkupInterface|string
   *   The route title as translated markup of string.
   */
  public function getTitle(EntityInterface $entity, string $field_name, string $view_mode) {
    $view_mode = $this->getViewMode($entity, $view_mode);
    /** @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface $view_display */
    $view_display = $this->entityTypeManager->getStorage('entity_view_display')
      ->load("{$entity->getEntityTypeId()}.{$entity->bundle()}.$view_mode");

    if (!$view_display || !($component = $view_display->getComponent($field_name))) {
      throw new \InvalidArgumentException("Parameters passed to 'email_contact.form' route are not defining a valid display or field.");
    }

    if (!empty($component['settings']['title']) && $title = trim($component['settings']['title'])) {
      return $title;
    }

    return $this->t("@label - Email Contact", ['@label' => $entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function content(EntityInterface $entity, ?string $field_name = NULL, string $view_mode = 'full') {
    if (!$entity instanceof ContentEntityBase) {
      throw new NotFoundHttpException();
    }
    $view_display = EntityViewDisplay::collectRenderDisplay($entity, $view_mode);
    $field_display = $view_display->getComponent($field_name);
    $field_settings = EmailContactLinkFormatter::defaultSettings();
    if (isset($field_display['settings'])) {
      $field_settings = $field_display['settings'];
    }

    $form = $this->getForm($entity, $field_name, $field_settings);

    if (!empty($field_settings['modal'])) {
      $response = new AjaxResponse();
      // Allow modules to alter or replace the Ajax command.
      $event = new AjaxEmailContactCommandEvent(new OpenModalDialogCommand(
        $this->getTitle($entity, $field_name, $view_mode),
        $form,
        ['width' => '700']
      ), [
        'instance' => 'email_contact.modal.open',
        'title' => $this->getTitle($entity, $field_name, $view_mode),
        'content' => $form,
      ]);
      $this->eventDispatcher->dispatch($event);
      $response->addCommand($event->getCommand())->addAttachments($event->getAttachments());
      return $response;
    }

    return $form;
  }

  /**
   * Gets a renderable form array.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The field host entity.
   * @param string $field_name
   *   The field name.
   * @param array $field_settings
   *   The field settings.
   *
   * @return array
   *   The form array.
   */
  protected function getForm(EntityInterface $entity, string $field_name, array $field_settings): array {
    return $this->formBuilder->getForm(new ContactForm(
      $entity->getEntityTypeId(),
      $entity->id(),
      $field_name,
      $field_settings
    ));
  }

}
