<?php

namespace Drupal\Tests\email_contact\FunctionalJavascript;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\email_contact\Traits\EmailContactTestTrait;

/**
 * @coversDefaultClass \Drupal\email_contact\Plugin\Field\FieldFormatter\EmailContactLinkFormatter
 * @group email_contact
 */
class EmailContactLinkFormatterTest extends WebDriverTestBase {

  use EmailContactTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'email_contact',
    'entity_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Whether to configure the formatter with modal.
   *
   * @var bool
   */
  protected $modal = TRUE;

  /**
   * Tests the formatter.
   */
  public function testFormatter(): void {
    $assert = $this->assertSession();
    $this->clickLink('Contact person by email');
    $assert->waitForButton('Send e-mail');
    $assert->elementTextEquals('css', '.ui-dialog-title', "{$this->entity->label()} - Email Contact");
    $edit = [
      'name' => $this->randomString(),
      // Wrong email.
      'mail' => $this->randomMachineName(),
      'subject' => $this->randomString(),
      'message' => $this->randomString(),
    ];
    $this->submitForm($edit, 'Send e-mail');
    $assert->waitForText('You must enter a valid e-mail address.');

    // Set a valid email and retry.
    $edit['mail'] = $this->randomMachineName() . '@example.com';
    $this->submitForm($edit, 'Send e-mail');

    $assert->assertNoElementAfterWait('css', '.ui-dialog-title');
    $assert->statusMessageContains('Your message has been sent.');
    $assert->addressEquals($this->entity->toUrl());
    $this->assertEmail($edit['mail'], $edit['name'], $this->entity->get('email')->value, $edit['subject'], $edit['message']);

    // Change some formatter settings.
    EntityViewDisplay::load('entity_test.entity_test.full')
      ->setComponent('email', [
        'type' => 'email_contact_link',
        // Note that some settings, such as redirection_to, custom_path, are not
        // editable in UI, thus are not tested.
        'settings' => [
          'include_values' => FALSE,
          'default_message' => $default_message = $this->randomString(),
          'link_text' => $link_text = $this->randomString(),
          'title' => $title = $this->randomString(),
          'modal' => $this->modal,
        ],
      ])->save();

    $this->drupalGet($this->entity->toUrl());
    $this->clickLink($link_text);
    $assert->waitForButton('Send e-mail');
    $assert->elementTextEquals('css', '.ui-dialog-title', strip_tags($title));

    $this->submitForm($edit, 'Send e-mail');
    $assert->assertNoElementAfterWait('css', '.ui-dialog-title');
    $assert->statusMessageContains('Your message has been sent.');
    $assert->addressEquals($this->entity->toUrl());

    $this->assertEmail($edit['mail'], $edit['name'], $this->entity->get('email')->value, $edit['subject'], $edit['message'], FALSE, $default_message);
  }

}
