<?php

namespace Drupal\Tests\email_contact\Traits;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Test\AssertMailTrait;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Tests reusable code.
 */
trait EmailContactTestTrait {

  use AssertMailTrait;

  /**
   * Testing entity.
   *
   * @var \Drupal\entity_test\Entity\EntityTest
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    FieldStorageConfig::create([
      'type' => 'email',
      'entity_type' => 'entity_test',
      'field_name' => 'email',
    ])->save();
    FieldConfig::create([
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
      'field_name' => 'email',
      'label' => 'Email',
    ])->save();
    EntityViewDisplay::create([
      'targetEntityType' => 'entity_test',
      'bundle' => 'entity_test',
      'mode' => 'full',
      'status' => TRUE,
    ])->setComponent('email', [
      'type' => 'email_contact_link',
      'settings' => ['modal' => $this->modal],
    ])->save();

    $this->entity = EntityTest::create([
      'type' => 'entity_test',
      'name' => $this->randomString(),
      'email' => $this->randomMachineName() . '@example.com',
    ]);
    $this->entity->save();

    $this->drupalLogin($this->createUser(['view test entity', 'access content']));
    $this->drupalGet($this->entity->toUrl());
  }

  /**
   * Asserts that a message has been sent via Email Contact form.
   *
   * @param string $from
   *   The sender's email.
   * @param string $from_name
   *   The sender's name.
   * @param string $to
   *   The recipient's email.
   * @param string $subject
   *   The email's subject.
   * @param string $body
   *   The email's body.
   * @param bool $include_values
   *   (optional) Whether to include the values. Defaults to TRUE.
   * @param string $default_message
   *   (optional) The default message. Defaults to ''.
   */
  protected function assertEmail(string $from, string $from_name, string $to, string $subject, string $body, bool $include_values = TRUE, string $default_message = ''): void {
    $email = $this->getMails(['id' => 'email_contact_contact'])[0];
    $this->assertSame($from, $email['reply-to']);
    $this->assertSame($from_name, $email['params']['name']);
    $this->assertSame($to, $email['to']);
    // Plain subject is processed from HTML to plain.
    // @see \Drupal\Core\Mail\MailManager::doMail()
    $this->assertSame(PlainTextOutput::renderFromHtml($subject), $email['subject']);
    $this->assertSame($default_message, $email['params']['default_message']);

    // Note that this 'include values' way of composing many variables in the
    // mail body is hardcoded in ContactForm::sendMessage() and it's a wrong
    // approach. Instead, a template should be provided, allowing projects to
    // override with their own approach. It also looks superfluous as all the
    // information are already part of the email's header.
    // @see \Drupal\email_contact\Form\ContactForm::sendMessage()
    $body = "<br/>Message: $body";
    $body = $include_values ? "Name: $from_name<br/>Email: $from<br/>$body" : $body;
    $this->assertEquals($body, $email['params']['message']->__toString());
    // Prepare the test email collector for the next assertion.
    $state = \Drupal::state();
    $state->set('system.test_mail_collector', []);
    $state->resetCache();
  }

}
