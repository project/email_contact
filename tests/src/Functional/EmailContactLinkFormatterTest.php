<?php

namespace Drupal\Tests\email_contact\Functional;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\email_contact\Traits\EmailContactTestTrait;

/**
 * @coversDefaultClass \Drupal\email_contact\Plugin\Field\FieldFormatter\EmailContactLinkFormatter
 * @group email_contact
 */
class EmailContactLinkFormatterTest extends BrowserTestBase {

  use EmailContactTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'email_contact',
    'entity_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Whether to configure the formatter with modal.
   *
   * @var bool
   */
  protected $modal = FALSE;

  /**
   * Tests the formatter.
   */
  public function testFormatter(): void {
    $assert = $this->assertSession();
    $this->clickLink('Contact person by email');
    $assert->elementTextEquals('css', 'h1', "{$this->entity->label()} - Email Contact");
    $edit = [
      'name' => $this->randomString(),
      'mail' => $this->randomMachineName() . '@example.com',
      'subject' => $this->randomString(),
      'message' => $this->randomString(),
    ];
    $this->submitForm($edit, 'Send e-mail');
    $this->assertEmail($edit['mail'], $edit['name'], $this->entity->get('email')->value, $edit['subject'], $edit['message']);

    // Change some formatter settings.
    EntityViewDisplay::load('entity_test.entity_test.full')
      ->setComponent('email', [
        'type' => 'email_contact_link',
        // Note that some settings, such as redirection_to, custom_path, are not
        // editable in UI, thus are not tested.
        'settings' => [
          'include_values' => FALSE,
          'default_message' => $default_message = $this->randomString(),
          'link_text' => $link_text = $this->randomString(),
          'title' => $title = $this->randomString(),
          'modal' => $this->modal,
        ],
      ])->save();

    $this->drupalGet($this->entity->toUrl());
    $this->clickLink($link_text);
    $assert->elementTextEquals('css', 'h1', $title);

    $this->submitForm($edit, 'Send e-mail');
    $this->assertEmail($edit['mail'], $edit['name'], $this->entity->get('email')->value, $edit['subject'], $edit['message'], FALSE, $default_message);
  }

}
